# GitLab GitOps Demo

This project can be used to demonstrate a GitOps pipeline that showcases features of Ultimate. It follows a similar workflow to a typical day-in-the-life development demo while integrating best of breed tools for Infrastructure as Code and configuration management.

This is a fairly concise guide with important information regarding setup, security, and operation; please read it in full.

## Quick-start guide

Export this project and import as one of your own. A pipeline will run in the main branch to build your builder image.

Set the following CI/CD variables in _Settings → CI/CD → Variables_:

* `AWS_ACCESS_KEY_ID`
* `AWS_SECRET_ACCESS_KEY`
* `AWS_DEFAULT_REGION`

These variables will be shared by AWS CLI and Terraform. You're advised to create an ad hoc, least-privilege user in your AWS account with only the permissions needed to deploy the resources in your Terraform templates. Access to your project and the variable for your AWS secret access key should be restricted. In a future iteration, [support for user credentials will be deprecated](https://gitlab.com/marquard/gitlab-iac-demo/-/issues/45).

## Components of the project

### Builder image

This project contains pipelines for building and supporting your own builder image that will be used for running GitOps pipelines. Although the image must be built on a Runner with Internet access, the image itself can be run in [offline environments](https://docs.gitlab.com/ee/user/application_security/offline_deployments/) for demos that might need to be delivered in a SCIF.

#### Customizing your builder image

To iterate on your builder image, use the ~"dev:image" label in your Merge Request. If you create an MR from an issue that has this label, your MR will inherit the label. This will run an image build job on MR events, pushing to your container registry with a tag consistent with the name of the branch. Merging your changes into the main branch will trigger a pipeline to release the image, pushing to the container registry with the `latest` tag.

### Web app

### GitOps pipeline
